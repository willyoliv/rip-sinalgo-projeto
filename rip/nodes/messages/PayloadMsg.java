package projects.projetorip.nodes.messages;

import sinalgo.nodes.Node;
import sinalgo.nodes.messages.Message;

/**
 * A message used to transport data from a sender to a receiver
 */
public class PayloadMsg extends Message {
	public Node destination; // node who should receive this msg
	public Node sender; // node who sent this msg
	public int sequenceNumber; // a number to identify this msg, set by the sender 
	public int ttl = 0;
	
	public PayloadMsg(Node destination, Node sender) {
		this.destination = destination;
		this.sender = sender;
	}
	
	@Override
	public Message clone() {
		return this; // requires the read-only policy
	}

}
