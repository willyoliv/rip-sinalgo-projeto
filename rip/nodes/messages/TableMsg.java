package projects.projetorip.nodes.messages;
import sinalgo.nodes.Node;
import sinalgo.nodes.messages.Message;
import projects.projetorip.nodes.nodeImplementations.No.TabelaRoteamento;



public class TableMsg extends Message {
	
	/**
	 * Constructor
	 * @param destination The node to whom the ACK should be sent
	 * @param sender The node that acknowledges receipt of a message
	 */
	public Node destination;
	public Node sender;
	public TabelaRoteamento tabela;

	public TableMsg(Node destination, Node sender, TabelaRoteamento tabela) {
		this.destination = destination;
		this.sender = sender;
		this.tabela = tabela;
	}

	@Override
	public Message clone() {
		return this; // read-only policy
	}

}
