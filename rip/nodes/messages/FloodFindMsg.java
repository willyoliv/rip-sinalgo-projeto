package projects.projetorip.nodes.messages;

import projects.projetorip.nodes.nodeImplementations.No.TabelaRoteamento;
import projects.projetorip.nodes.timers.RetryFloodingTimer;
import sinalgo.nodes.Node;
import sinalgo.nodes.messages.Message;

public class FloodFindMsg extends Message {

	public Node sender;
	public int sequenceID; 
	public RetryFloodingTimer retryTimer = null;
	public Node destination;
	public TabelaRoteamento tabela;
	public boolean chegou = false;
	
	public FloodFindMsg(int seqID, Node sender, Node dest,TabelaRoteamento tabela) {
		sequenceID = seqID;
		this.sender = sender;
		destination = dest;
		this.tabela = tabela;
	}
	
	@Override
	public Message clone() {
		// This message requires a read-only policy
		return this;
	}
	public FloodFindMsg getRealClone() {
		FloodFindMsg msg = new FloodFindMsg(this.sequenceID, this.sender, this.destination, this.tabela);
		msg.tabela = this.tabela;
		msg.retryTimer = this.retryTimer;
		return msg;
	}

}
