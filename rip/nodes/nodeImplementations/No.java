package projects.projetorip.nodes.nodeImplementations;

import java.awt.Color;
import java.util.ArrayList;


import projects.defaultProject.models.messageTransmissionModels.ConstantTime;
import projects.projetorip.nodes.messages.TableMsg;
import projects.projetorip.nodes.messages.ConnectMsg;
import projects.projetorip.nodes.messages.FloodFindMsg;
import projects.projetorip.nodes.messages.PayloadMsg;
import projects.projetorip.nodes.timers.NoDesativo;
import projects.projetorip.nodes.timers.PayloadMessageTimer;
import projects.projetorip.nodes.timers.RetryFloodingTimer;
import projects.projetorip.nodes.timers.SendConnect;
import projects.projetorip.nodes.nodeImplementations.No;
import sinalgo.configuration.WrongConfigurationException;
import sinalgo.gui.helper.NodeSelectionHandler;
import sinalgo.nodes.Node;
import sinalgo.nodes.messages.Inbox;
import sinalgo.nodes.messages.Message;
import sinalgo.tools.Tools;
import sinalgo.nodes.Connections;
import sinalgo.nodes.edges.Edge;

/**
 * A node that implements a flooding strategy to determine paths to other nodes.
 */
public class No extends Node {

	public class TabelaRoteamento{
		public ArrayList<Node> destino = new ArrayList<Node>();
		public ArrayList<Integer> numSalto = new ArrayList<Integer>();
		public ArrayList<Node> proxSalto = new ArrayList<Node>();
		public ArrayList<Integer> tempVizinho = new ArrayList<>();
		public ArrayList<Node> vizinhos = new ArrayList<>();
		
		public TabelaRoteamento() {
			
		}
		public TabelaRoteamento(Connections conexao) {
			for(Edge edge: conexao) {
				this.destino.add(edge.endNode);
				this.numSalto.add(1);
				this.tempVizinho.add(180);
				this.proxSalto.add(edge.endNode);
				this.vizinhos.add(edge.endNode);
			}
			
		}
		public void saltoUpdate(Node no, int qtdSalto) {
			int index = destino.indexOf(no);
			if(index != -1) {
				this.numSalto.set(index, qtdSalto);
			}
		} 

		public void proxSaltoUpdate(Node no, Node proxSalto) {
			int index = destino.indexOf(no);
			if(index != -1) {
				this.proxSalto.set(index, proxSalto);
			}
		} 
		public Node getProxSalto(Node no) {
			int index = destino.indexOf(no);
			if(index != -1) {
				return this.proxSalto.get(index);
			} else {
				return null;
			}
		}
		
		public int getNumSalto(Node no) {
			int index = destino.indexOf(no);
			if(index != -1) {
				return this.numSalto.get(index);
			} else {
				return 0;
			}
		}
		public void resetTimer(Node no) {
			int index = vizinhos.indexOf(no);
			if(index != -1) {
				tempVizinho.set(index, 180);
			}
		}
	}
	
	public int seqID = 0; 
	public static int cont = 1;
	public static int timer = 0;
	public TabelaRoteamento tabela = new TabelaRoteamento(this.outgoingConnections);
	public void clearRoutingTable() {
		tabela = new TabelaRoteamento();
	}
	public void iniciarNos() {
		tabela = new TabelaRoteamento(this.outgoingConnections);
		RetryFloodingTimer r = new RetryFloodingTimer();
		r.startRelative(30+(timer+=1), this);
		NoDesativo in = new NoDesativo(this);
		in.startRelative(1,this);
	}

	@NodePopupMethod(menuText = "Inicializar N�")
	public void iniciarNo() {
		tabela = new TabelaRoteamento(this.outgoingConnections);
		RetryFloodingTimer r = new RetryFloodingTimer();
		ConnectMsg msg = new ConnectMsg(this);
		SendConnect sc = new SendConnect(msg, this);
		sc.startRelative(1,this);
		r.startRelative(30+(timer+=1), this);
		NoDesativo in = new NoDesativo(this);
		in.startRelative(1,this);
	}
	
	@Override
	public void checkRequirements() throws WrongConfigurationException {
		// The message delivery time must be constant, this allows the project
		// to easily predict the waiting times
		if(!(Tools.getMessageTransmissionModel() instanceof ConstantTime)) {
			Tools.fatalError("This project requires that messages are sent with the ConstantTime MessageTransmissionModel.");
		}
	}

	@Override
	public void handleMessages(Inbox inbox) {
		while(inbox.hasNext()) {
			Message msg = inbox.next();

			if(msg instanceof ConnectMsg) {
				ConnectMsg m = (ConnectMsg) msg;
				if(this.tabela.vizinhos.indexOf(m.sender) == -1 ) {
					if(this.tabela.destino.indexOf(m.sender) == -1){
						this.tabela.destino.add(m.sender);
						this.tabela.vizinhos.add(m.sender);
						this.tabela.numSalto.add(1);
						this.tabela.proxSalto.add(m.sender);
						this.tabela.tempVizinho.add(180);
					} else {
						this.tabela.vizinhos.add(m.sender);
						this.tabela.numSalto.set(this.tabela.destino.indexOf(m.sender),1);
						this.tabela.proxSaltoUpdate(m.sender,m.sender);
						this.tabela.tempVizinho.add(180);

					}
				}
				TableMsg tm = new TableMsg(m.sender, this, this.tabela);
				send(tm, m.sender);
			}
			
			if(msg instanceof TableMsg) {
				
				TableMsg m = (TableMsg) msg;
				if(this.tabela.vizinhos.indexOf(m.sender) == -1) {
					if(this.tabela.destino.indexOf(m.sender) == -1){
						this.tabela.destino.add(m.sender);
						this.tabela.vizinhos.add(m.sender);
						this.tabela.numSalto.add(1);
						this.tabela.proxSalto.add(m.sender);
						this.tabela.tempVizinho.add(180);
					} else {
						this.tabela.vizinhos.add(m.sender);
						this.tabela.numSalto.set(this.tabela.destino.indexOf(m.sender),1);
						this.tabela.proxSaltoUpdate(m.sender,m.sender);
						this.tabela.tempVizinho.add(180);

					}
				} 
				TabelaRoteamento rt = m.tabela;
				for(Node n : rt.destino) {
					int index = this.tabela.destino.indexOf(n);
					if(index == -1 && !this.equals(n)) {
						this.tabela.destino.add(n);
						this.tabela.numSalto.add(rt.getNumSalto(n) + 1);
						this.tabela.proxSalto.add(m.sender);

					} else if(rt.getNumSalto(n) < this.tabela.getNumSalto(n)) { 
						this.tabela.saltoUpdate(n, rt.getNumSalto(n) + 1);
						this.tabela.proxSaltoUpdate(n, m.sender);
					}
				}
				FloodFindMsg b = new FloodFindMsg(++this.seqID, No.this, m.sender, this.tabela);
				this.broadcast(b);

			}
			// ---------------------------------------------------------------
			if(msg instanceof FloodFindMsg) { // This node received a flooding message. 
				// ---------------------------------------------------------------
				FloodFindMsg m = (FloodFindMsg) msg;
				if(this.tabela.vizinhos.indexOf(m.sender) == -1) {
					this.tabela.destino.add(m.sender);
					this.tabela.vizinhos.add(m.sender);
					this.tabela.numSalto.add(1);
					this.tabela.proxSalto.add(m.sender);
					this.tabela.tempVizinho.add(180);
				} else {
					this.tabela.resetTimer(m.sender);
				}
				boolean forward = false;
				if(m.destination.equals(this)) {
					forward = false; 
				} else { 
					TabelaRoteamento rt = m.tabela;
					for(Node n : rt.destino) {
						int index = this.tabela.destino.indexOf(n);
						if(index == -1 && !this.equals(n)) { 
							this.tabela.destino.add(n);
							this.tabela.numSalto.add(rt.getNumSalto(n) + 1);
							this.tabela.proxSalto.add(m.sender);
							forward = true;
						} else if(rt.getNumSalto(n) < this.tabela.getNumSalto(n) || this.tabela.vizinhos.indexOf(this.tabela.getProxSalto(n)) == -1) { // update the existing entry 
							this.tabela.saltoUpdate(n, rt.getNumSalto(n) + 1);
							this.tabela.proxSaltoUpdate(n, m.sender);
							forward = true;
						}
						if(m.chegou && rt.getNumSalto(n) == 16 && this.tabela.vizinhos.indexOf(n) == -1) {
							this.tabela.saltoUpdate(n,16);
							this.tabela.proxSaltoUpdate(n, m.sender);
							forward = true;

						}
					}
				}


				if(forward) { 
					FloodFindMsg b = new FloodFindMsg(++this.seqID, No.this, m.sender, this.tabela);
					if(m.chegou){ b.chegou = true;}
					this.broadcast(b);
				}
			} 
			// ---------------------------------------------------------------
			if(msg instanceof PayloadMsg) {
				PayloadMsg m = (PayloadMsg) msg;
				if(m.destination.equals(this)) { 
					this.setColor(Color.YELLOW);			
				} else {
					if(m.ttl < 15) {
						m.ttl++;
						this.setColor(Color.GREEN);
						sendPayloadMessage(m);
					}
				}
			} 
		}
	}
	@NodePopupMethod(menuText = "Send Message To...")
	public void sendMessageTo() {
		Tools.getNodeSelectedByUser(new NodeSelectionHandler() {
			public void handleNodeSelectedEvent(Node n) {
				if(n == null) {
					return; // aborted
				}
				PayloadMsg msg = new PayloadMsg(n, No.this);
				msg.sequenceNumber = ++No.this.seqID;
				PayloadMessageTimer t = new PayloadMessageTimer(msg);
				t.startRelative(1, No.this);
				No.this.setColor(Color.GREEN);
			}
		}, "Select a node to send a message to...");
	}
	
	public void sendPayloadMessage(PayloadMsg msg) {
		Node nh = this.tabela.getProxSalto(msg.destination);
		if(nh != null) {
			if(this.tabela.getNumSalto(msg.destination) - 15 <= msg.ttl) {
				send(msg, nh);
			} else {
				System.out.println("n�mero de saltos excedido");
			}
			return ;
		} else {
			System.out.println("n�mero de saltos excedido");
		}
	}
	
	/**
	 * Starts a search for a given node with the given TTL
	 * @param destination
	 * @param ttl
	 */
	public void lookForNode() {
		FloodFindMsg m = new FloodFindMsg(++this.seqID, this, this, this.tabela);
		RetryFloodingTimer rft = new RetryFloodingTimer();
		rft.startRelative(30, this); 
		m.retryTimer = rft;
		this.broadcast(m);
	}
	public void shareInactiveNode(Node node) {
		for(Node n : this.tabela.destino) {
			if(this.tabela.getProxSalto(n).equals(node)) {
				this.tabela.saltoUpdate(n, 16);
				this.tabela.proxSaltoUpdate(n, null);				
			}
		}
		this.tabela.saltoUpdate(node, 16);
		this.tabela.vizinhos.set(this.tabela.vizinhos.indexOf(node), null);
		this.tabela.proxSaltoUpdate(node, null);
		FloodFindMsg m = new FloodFindMsg(++this.seqID, this, this, this.tabela);
		m.chegou = true;
		this.broadcast(m);
	}
	@Override
	public void preStep() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void init() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void neighborhoodChange() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void postStep() {
		// TODO Auto-generated method stub
		
	}
}
