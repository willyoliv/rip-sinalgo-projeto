package projects.projetorip.nodes.timers;

import projects.projetorip.nodes.messages.PayloadMsg;
import projects.projetorip.nodes.nodeImplementations.No;
import sinalgo.nodes.timers.Timer;

/**
 * A timer to initially send a message. This timer
 * is used in synchronous simulation mode to handle user
 * input while the simulation is not running. 
 */
public class PayloadMessageTimer extends Timer {
	PayloadMsg msg = null; // the msg to send

	/**
	 * @param msg The message to send
	 */
	public PayloadMessageTimer(PayloadMsg msg) {
		this.msg = msg;
	}
	
	@Override
	public void fire() {
		((No) node).sendPayloadMessage(msg);
	}

}
