package projects.projetorip.nodes.timers;

import projects.projetorip.nodes.nodeImplementations.No;
import sinalgo.nodes.Node;
import sinalgo.nodes.timers.Timer;

public class NoDesativo extends Timer {
	

	boolean noAtivo = true; 
	NoDesativo noDesativo = null;
	Node node;

	public void desativarNo() {
		noAtivo = false;
	}
	

	public NoDesativo(Node node) {
		this.node = node;
		this.noAtivo = true;
	}

	@Override
	public void fire() {
		if(noAtivo){
			No n = (No) this.node;
			for(Node no : n.tabela.vizinhos) {
				if(no != null) {
					int index = n.tabela.vizinhos.indexOf(no);
					int aux = n.tabela.tempVizinho.get(index) - 1;
					n.tabela.tempVizinho.set(index, aux);
					if(aux == 0) {
						n.shareInactiveNode(no);
						System.out.println("Conex�o Perdida "+n);	
					}
				}
			}
			noDesativo = new NoDesativo(this.node);
			noDesativo.startRelative(1,this.node);
		}
	}
}
